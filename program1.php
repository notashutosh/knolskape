<?php

// $input = array(array(1,2, array(3, array(1000,1001))), array(4,10));
$input = array(1,array(2,array(1,2,3)),array(2,array(1,2,3)));
$maxLevels = 2;
$currentLevel = 1;
$output = [];
printArray($input);
ob_start();
var_dump($output);
$contents = ob_get_contents();
ob_end_clean();
echo($contents);
function printArray($input) {
	global $output;
	global $currentLevel;
	global $maxLevels;
	echo "CURRENT LEVEL $currentLevel\n";
	$currentLevel++;
	foreach($input as $element) {
		if(gettype($element) == "array" && $currentLevel <= $maxLevels) {
			printArray($element);
			$currentLevel--;
		}
		else {
			$output[] = $element;
		}
	}
}