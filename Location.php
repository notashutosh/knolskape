<?php

class Location {
	private $latitude;
	private $longitude;
	private $RADIUS_OF_EARTH = 6371000; //Radius of the earth 

	//We convert degrees into radians
	public function __construct($latitude, $longitude) {
		if(gettype($latitude) !== "double" || gettype($longitude) !== "double") {
			$latitude = floatval($latitude);
			$longitude = floatval($longitude);
		}
		$this->latitude = deg2rad($latitude);
		$this->longitude = deg2rad($longitude);
	}

	public function getDistanceFrom(Location $otherLocation) {
		$phi1 = $this->latitude;
		$lamda1 = $this->longitude;

		$phi2 = $otherLocation->latitude;
		$lamda2 = $otherLocation->longitude;

		$delPhi = $phi1 - $phi2;
		$delLamda = $lamda1 - $lamda2;

		$term1 = pow(sin($delPhi/2), 2) + (cos($phi1) * cos($phi2) * sin($delLamda/2) * sin($delLamda/2));
		$term2 = 2 * atan2(sqrt($term1), sqrt(1-$term1));

		return ($term2 * $this->RADIUS_OF_EARTH)/1000;
	}

	public function getLatitude() {
		return $this->latitude;
	}

	public function getLongitude() {
		return $this->longitude;
	}
}