<?php

function autoload_class($class){
    include_once('./' . $class . '.php');
}
spl_autoload_register('autoload_class');

/* SIMPLE TEST CASES USING ASSERTIONS */

//checks that two locations give the same distance
$knolskape = new Location(12.9754117, 77.6359441);
$sampleLocation = new Location(12.9754117, 77.6359441);
assert($knolskape->getDistanceFrom($knolskape) == 0);
assert($sampleLocation->getDistanceFrom($knolskape) == 0);

//non numeric inputs will be set to 0
$badLocation = new Location("hi","howareyou");
assert($badLocation->getLatitude() == 0);

//benchmark with a link that does this calculation (found via. google search)
//http://www.movable-type.co.uk/scripts/latlong.html
$locationInDelhi = new Location(28.61394, 77.20902);
assert(round($locationInDelhi->getDistanceFrom($knolskape),0) == 1739);


/* THE TASK ITSELF */
$inputJson = '{
  "A": { "Latitude":12.93742, "Longitude":77.62439, "City":"Bangalore"},
  "C": { "Latitude":12.97189, "Longitude":77.64115, "City":"Bangalore"},
  "B": { "Latitude":13.08268, "Longitude":80.27072, "City":"Chennai"},
  "D": { "Latitude":13.03577, "Longitude":77.59702, "City":"Bangalore"},
  "E": { "Latitude":9.9252, "Longitude":78.11978, "City":"Madurai"},
  "F": { "Latitude":12.89786, "Longitude":77.58451, "City":"Bangalore"},
  "G": { "Latitude":28.61394, "Longitude":77.20902, "City":"Delhi"},
  "H": { "Latitude":12.9754117, "Longitude":77.6359441, "City":"Bangalore/Knolskape"}
}';

$userData = json_decode($inputJson, true);

krsort($userData); // descending alphabetical order

$knolskape = new Location(12.9754117, 77.6359441);
foreach ($userData as $userName => $userData) {
	echo "User's name:\t".$userName."<br>";
	echo "Latitude:\t".$userData['Latitude']."<br>";
	echo "Longitude:\t".$userData['Longitude']."<br>";
	echo "City:\t".$userData['City']."<br>";

	$userLocation = new Location($userData['Latitude'], $userData['Longitude']);
	$distanceFromKnolskape = $userLocation->getDistanceFrom($knolskape);
	echo "Distance from Knolskape:\t".round($distanceFromKnolskape,2)." km";
	if($distanceFromKnolskape <= 100) {
		echo "<b> INVITED FOR EVENT </b><br>";
	} else {
		echo " NOT INVITED FOR EVENT<br>";
	}
	echo "<br><br>";
}




